<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Logs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('st_acao'); 
            $table->string('st_browser'); 
            $table->string('st_url');
            $table->string('st_ip');
            $table->string('st_msg'); 
            $table->string('st_antes'); 
            $table->string('st_depois'); 
            $table->string('ce_tipo'); 
            $table->string('ce_usuario'); 
            $table->string('dt_acao'); 
            $table->string('ce_registro');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('logs');
    }
}
