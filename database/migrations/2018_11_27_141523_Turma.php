<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Turma extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('turma', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome', 50)->unique();
            $table->integer('ano_id');
            $table->date('ini');
            $table->date('ter');
            $table->date('ini_pg');
            $table->date('ter_pg');
            $table->integer('bo_ativo')->default(1);
            $table->foreign('ano_id')->references('id')->on('ano');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('turma');
    }
}
