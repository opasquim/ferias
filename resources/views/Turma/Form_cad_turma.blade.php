@extends('adminlte::page')

@section('title', 'Cadastro de Turma')


@section('content')
<div class="row">
        <div class="col-md-12">
            
                @if(session('sucessoMsg'))
                <div class="container">
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <strong>Sucesso!</strong> {{ session('sucessoMsg')}}
                    </div>
                </div>
                @endif
                @if(session('erroMsg'))
                <div class="container">
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <strong>Atenção!</strong> {{ session('erroMsg')}}
                    </div>
                </div>
                @endif
        </div>
    </div>
<div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-primary">
                    <div class="panel-heading">Cadastro Turma</div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ url('/home/turma') }}">
                            {{ csrf_field() }}
                            <div class="form-group{{ $errors->has('nome') ? ' has-error' : '' }}">
                                <label for="nome" class="col-md-4 control-label">Nome</label>
    
                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" required="true" placeholder="Nome da Turma" name="nome" value="{{ old('nome') }}"> 
                                    @if ($errors->has('nome'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('nome') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            
                            <div class="form-group{{ $errors->has('ano_id') ? ' has-error' : '' }}">
                                <label for="ano_id" class="col-md-4 control-label">Ano</label>
    
                                <div class="col-md-6">
                                    <select id="ano_id" type="text" class="form-control" required="true" placeholder="ano_id" name="ano_id" value="{{ old('ano_id') }}">
                                    @if(isset($anos))
                                        @foreach($anos as $a)
                                            <option value = "{{$a->id}}">{{$a->nome}} {{$a->orgao}}</option>
                                        @endforeach
                                    @endif
                                    </select>
                                    @if ($errors->has('ano_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('ano_id') }}</strong>
                                    </span>
                                    @endif    
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('ini') ? ' has-error' : '' }}">
                                <label for="ini" class="col-md-4 control-label">Início da publicação</label>
    
                                <div class="col-md-6">
                                    <input id="ini" type="date" class="form-control" required="true" name="ini" value="{{ old('ini') }}"> 
                                    @if ($errors->has('ini'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('ini') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('ter') ? ' has-error' : '' }}">
                                <label for="ter" class="col-md-4 control-label">Término da publicação</label>
    
                                <div class="col-md-6">
                                    <input id="ter" type="date" class="form-control" required="true" name="ter" value="{{ old('ter') }}"> 
                                    @if ($errors->has('ter'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('ter') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('ini_pg') ? ' has-error' : '' }}">
                                <label for="ini_pg" class="col-md-4 control-label">Início do pagamento</label>
    
                                <div class="col-md-6">
                                    <input id="ini_pg" type="date" class="form-control" required="true" name="ini_pg" value="{{ old('ini_pg') }}"> 
                                    @if ($errors->has('ini_pg'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('ini_pg') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('ter_pg') ? ' has-error' : '' }}">
                                <label for="ter_pg" class="col-md-4 control-label">Término do pagamento</label>
    
                                <div class="col-md-6">
                                    <input id="ter_pg" type="date" class="form-control" required="true" name="ter_pg" value="{{ old('ter_pg') }}"> 
                                    @if ($errors->has('ter_pg'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('ter_pg') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group ">
                                <div class="col-md-2  col-md-offset-4">
                                    <a href="javascript:history.back()" class=" btn btn-danger"  title="Voltar">
                                        <span class="glyphicon glyphicon-arrow-left"></span> Voltar
                                    </a>
                                </div>
                                <button type="submit" class="col-md-2 btn btn-primary">
                                    <i class="fa fa-btn fa-file"></i> Cadastrar
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    
@stop