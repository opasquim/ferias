@extends('adminlte::page')

@section('title', 'Lista de Turmas')

@section('content')
<a href="{{url('turma/create')}}" class="btn btn-primary">Adicionar nova turma</a>
    <div class="content">
        <div class="row">
            <div class="col-md-12">

                <table class="table table-bordered table-responsive no-padding">
                    <thead>
                        <tr class="bg-primary">
                            <th colspan="5">LISTA DE TURMAS</th>
                        </tr>
                        <tr>
                            <th class="col-md-4">NOME</th>
                            <th class="col-md-2">ANO</th>
                            <th class="col-md-2">INÍCIO</th>
                            <th class="col-md-2">TÉRMINO</th>
                            <th class="col-md-2">AÇÕES</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(isset($turmas))
                            @forelse($turmas as $t)
                            <tr>
                                <th>{{$t->nome}}</th>
                                <th>{{$t->ano->nome}}</th>
                                <th>{{$t->ini}}</th>
                                <th>{{$t->ter}}</th>
                                <th>
                                    <a class="btn btn-primary" href="{{url('#')}}">Editar</a> | 
                                    <a onclick="modalDesativa({{$t->id}})" data-toggle="modal" data-placement="top" title="Excluir Turma" class="btn btn-danger">
                                    <span class="glyphicon glyphicon-ban-circle"></span></a> 
                                </th>
                             
                            </tr>
                            @empty
                            <tr>
                                <td colspan="5" style="text-align: center;">Nenhuma turma registrada</td>
                            </tr>
                            @endforelse
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <!--Modal Desativa usuário -->
    <div class="modal fade-lg" id="Modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog  modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Excluir Turma</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form class="form-inline" id="modalDesativa" method="post" > {{csrf_field()}}
                    <div class="modal-body bg-danger">
                        <h4 class="modal-title" id="exampleModalLabel">
                            <b>DESEJA REALMENTE EXCLUIR A TURMA?</b>
                        </h4>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary">Excluir</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script>
        function modalDesativa(id){
            $("#modalDesativa").attr("action", "{{ url('#')}}/");
            $('#Modal').modal();        
        };
    </script>

@stop