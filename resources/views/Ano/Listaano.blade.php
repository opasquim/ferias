@extends('adminlte::page')

@section('title', 'Lista de Anos')

@section('content')
<a href="{{url('ano/create')}}"><h1 class="btn btn-primary">Adicionar novo ano</h1></a>
    <div class="content">
        <div class="row">
            <div class="col-md-12">

                <table class="table table-bordered">
                    <thead>
                        <tr class="bg-primary">
                            <th colspan="6">LISTA DE ANOS</th>
                        </tr>
                        <tr>
                            <th class="col-md-3">NOME</th>
                            <th class="col-md-5">ORGÃO</th>
                            <th class="col-md-2">AÇÕES</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(isset($anos))
                            @forelse($anos as $a)
                            <tr>
                                <th>{{$a->nome}}</th>
                                <th>{{$a->orgao}}</th>
                                <th>
                                    <a class="btn btn-primary" href="{{url('#')}}">Editar</a> | 
                                    <a onclick="modalDesativa({{$a->id}})" data-toggle="modal" data-placement="top" title="Excluir Ano" class="btn btn-danger">
                                    <span class="glyphicon glyphicon-ban-circle"></span></a> 
                                </th>
                             
                            </tr>
                            @empty
                            <tr>
                                <td colspan="4" style="text-align: center;">Nenhum ano registrado</td>
                            </tr>
                            @endforelse
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <!--Modal Desativa usuário -->
    <div class="modal fade-lg" id="Modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog  modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Excluir Ano</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form class="form-inline" id="modalDesativa" method="post" > {{csrf_field()}}
                    <div class="modal-body bg-danger">
                        <h4 class="modal-title" id="exampleModalLabel">
                            <b>DESEJA REALMENTE EXCLUIR O ANO?</b>
                        </h4>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary">Excluir</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script>
        function modalDesativa(id){
            $("#modalDesativa").attr("action", "{{ url('#')}}/");
            $('#Modal').modal();        
        };
    </script>

@stop