<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//Rotas para Ano
Route::get('/home/ano', 'AnoController@index');
Route::get('ano/create', 'AnoController@create');
Route::post('/home/ano', 'AnoController@store');

//Rotas para turmas
Route::get('/home/turma', 'TurmaController@index');
Route::get('turma/create', 'TurmaController@create');
Route::post('/home/turma', 'TurmaController@store');

//Rotas para Pessoas
Route::get('/home/pessoa', 'PessoaController@index');
Route::get('pessoa/create', 'PessoaController@create');
Route::post('/home/pessoa', 'PessoaController@store');