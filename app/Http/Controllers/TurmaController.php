<?php

namespace App\Http\Controllers;

use App\Turma;
use App\Ano;
use Illuminate\Http\Request;
use DB;
use App\utis\MyLog;
use Auth;
use Carbon\Carbon;

class TurmaController extends Controller{
    
      /*
    |--------------------------------------------------------------------------
    | LISTAGEM DE TURMAS
    |--------------------------------------------------------------------------
    */

    public function index(){
       
        $turmas = Turma::where('bo_ativo',  '1')
        ->orderby('id')->get();

        return view('Turma.Listaturma', compact('turmas')); 
    }

    public function create(){
        $anos = Ano::where('bo_ativo',  '1')
        ->orderby('nome')->get();

        return view('Turma.Form_cad_turma', compact('turmas','anos'));
    }

    public function store(Request $request)
    {
        //dd($request->all());
        DB:: beginTransaction();
        //recebendo os dados do formulário
        $dadosForm = $request->all();
        $turmas = new Turma;
        //validando os dados
        $validator = validator($dadosForm, [
            'nome' => 'required',
            'ano_id' => 'required',
            'ini' => 'required',
            'ter' => 'required',
            'ini_pg' => 'required',
            'ter_pg' => 'required'
        ]);
                 
        if($validator->fails()){
            return redirect()->back()
            //          Mensagem de Erro
                    ->withErrors($validator)
           //          Preenchendo o Formulário
                    ->withInput();
        }
        //Inserindo um usuário
        // $insert = User::create($dadosForm);
        $insert = Turma::create($dadosForm) ;
       
        if($insert){
            DB::commit();
            //chamando a classe para registra a alteração na tabela logs
            $acao = "Cadastro";
            $msg = 'O Usuário: ' . Auth::user()->name . ' cadastrou uma nova turma';
            MyLog::info(compact('antes', 'depois', 'msg', 'acao'));
            return redirect('/home/turma');
        }else{
            DB::rollback();
            //   return 'Falha ao Cadastrar os Dados!';
            return redirect()->back()->with('erroMsg', 'Falha ao Cadastrar a turma');  
        }
    }
}