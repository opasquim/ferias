<?php

namespace App\Http\Controllers;

use App\Ano;
use Illuminate\Http\Request;
use DB;
use App\utis\MyLog;
use Auth;
use Carbon\Carbon;

class AnoController extends Controller{
    
      /*
    |--------------------------------------------------------------------------
    | LISTAGEM DE ANOS
    |--------------------------------------------------------------------------
    */

    public function index(){
       
        $anos = Ano::where('bo_ativo',  '1')
        ->orderby('nome')->get();

       // dd($anos);

        return view('Ano.Listaano', compact('anos')); 
    }

     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Ano.Form_cad_ano', compact('anos'));
    }

    public function store(Request $request)
    {
        DB:: beginTransaction();
        //recebendo os dados do formulário
        $dadosForm = $request->all();
        $anos = new Ano;
        //validando os dados
        $validator = validator($dadosForm, [
            'nome' => 'required',
            'orgao' => 'required'
        ]);
                 
        if($validator->fails()){
            return redirect()->back()
            //          Mensagem de Erro
                    ->withErrors($validator)
           //          Preenchendo o Formulário
                    ->withInput();
        }
        //Inserindo um usuário
        // $insert = User::create($dadosForm);
        $insert = Ano::create($dadosForm) ;
       
        if($insert){
            DB::commit();
            //chamando a classe para registra a alteração na tabela logs
            $acao = "Cadastro";
            $msg = 'O Usuário: ' . Auth::user()->name . ' cadastrou um novo ano';
            MyLog::info(compact('antes', 'depois', 'msg', 'acao'));
            return redirect('/home/ano');
        }else{
            DB::rollback();
            //   return 'Falha ao Cadastrar os Dados!';
            return redirect()->back()->with('erroMsg', 'Falha ao Cadastrar o ano');  
        }
    }
}