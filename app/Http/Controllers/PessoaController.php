<?php

namespace App\Http\Controllers;

use App\Pessoa;
use Illuminate\Http\Request;
use DB;
use App\utis\MyLog;
use Auth;
use Carbon\Carbon;

class PessoaController extends Controller{
    
      /*
    |--------------------------------------------------------------------------
    | LISTAGEM DE PESSOAS
    |--------------------------------------------------------------------------
    */

    public function index(){
       
        $pessoas = Pessoa::where('bo_ativo',  '1')
        ->orderby('nome')->get();

        return view('Pessoa.Listapessoa', compact('pessoas')); 
    }

     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Pessoa.Form_cad_pessoa', compact('pessoas'));
    }

    public function store(Request $request)
    {
        DB:: beginTransaction();
        //recebendo os dados do formulário
        $dadosForm = $request->all();
        $pessoas = new Pessoa;
        //validando os dados
        $validator = validator($dadosForm, [
            'nome' => 'required',
            'cpf' => 'required',
            'matricula' => 'required'
        ]);
                 
        if($validator->fails()){
            return redirect()->back()
            //          Mensagem de Erro
                    ->withErrors($validator)
           //          Preenchendo o Formulário
                    ->withInput();
        }
        //Inserindo um usuário
        // $insert = User::create($dadosForm);
        $insert = Pessoa::create($dadosForm) ;
       
        if($insert){
            DB::commit();
            //chamando a classe para registra a alteração na tabela logs
            $acao = "Cadastro";
            $msg = 'O Usuário: ' . Auth::user()->name . ' cadastrou uma nova pessoa';
            MyLog::info(compact('antes', 'depois', 'msg', 'acao'));
            return redirect('/home/pessoa');
        }else{
            DB::rollback();
            //   return 'Falha ao Cadastrar os Dados!';
            return redirect()->back()->with('erroMsg', 'Falha ao Cadastrar pessoa');  
        }
    }
}