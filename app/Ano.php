<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ano extends Model
{
    protected $fillable = ['id','nome', 'orgao', 'bo_ativo'];

    public $timestamps = false;

    protected $table = 'ano';
}