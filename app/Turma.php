<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Turma extends Model
{
    protected $fillable = ['id', 'nome', 'ano_id', 'ini', 'ter', 'ini_pg', 'ter_pg', 'bo_ativo'];

    public $timestamps = false;

    protected $table = 'turma';

    public function ano()
    {
        return $this->belongsTo('App\Ano');
    }
}