<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pessoa extends Model
{
    protected $fillable = ['id','nome', 'cpf', 'matricula', 'bo_ativo'];

    public $timestamps = false;

    protected $table = 'pessoa';
}